# Task Instructions


If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Finish the solution so that it returns the sum of all the multiples of 3 or 5 below the number passed in. Additionally, if the number is negative, return 0 (for languages that do have them).

**Note: If the number is a multiple of both 3 and 5, only count it once.**

Courtesy of [projecteuler.net](https://projecteuler.net/ "projecteuler.net").

#### Git Instructions
- Create your own branch with Gitlab developer/{{username}}
- Push with full filed delivery note 
- Once commit don't forget to email along with latest commit 

### sincere Thanks to 
[![Gitlab](https://content.screencast.com/users/shribalaji3140/folders/public/media/4bc5b020-2bf9-406e-bb10-6ca19af6df46/gitlab.com.jpg "Gitlab")](https://gitlab.com "Gitlab")
[![www.codewars](https://content.screencast.com/users/shribalaji3140/folders/public/media/559997eb-1732-4f85-97dd-0d966bab724c/codewars.com.jpg "www.codewars")](https://www.codewars.com "www.codewars")
[![projecteuler.net](https://content.screencast.com/users/shribalaji3140/folders/public/media/1de5e6ed-1dab-43ae-954d-7980ba8f6ccc/projecteuler.net.jpg "projecteuler.net")](https://projecteuler.net "projecteuler.net")
[![stackoverflow.com](https://content.screencast.com/users/shribalaji3140/folders/public/media/97cfb42e-6f86-443e-8204-ecc198a233a7/stackoverflow.com.jpg "stackoverflow.com")](https://stackoverflow.com "stackoverflow.com")