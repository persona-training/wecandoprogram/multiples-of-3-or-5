const { assert } = require("chai");
const soluation = require('./index')

function test(n, expected) {
  let actual = soluation(n);
  it(`Expected ${expected}, got ${actual}`, () => {
    assert.strictEqual(actual, expected);
  });
}

describe("basic tests", function () {
  test(10, 23);
  test(15, 45);
  test(1000, 233168);
});
